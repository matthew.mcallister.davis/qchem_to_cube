template =\
"""{COMMENT1}
{COMMENT2}
{NATOMS} {ORIGIN} {NVAL}
{XAXIS}
{YAXIS}
{ZAXIS}
{GEOM}{DATA}"""
def lookup(nx,ny,nz,dset,NX,NY,NZ):
    return dset[nx*NY*NZ + ny*NZ + nz]

def create_data_ostr(dataset,NX,NY,NZ):
    ostr = ""
    NVAL = 1
    for i in range(NX):
        for j in range(NY):
            for k in range(NZ):
                ostr += "{:0.7e} ".format(lookup(i,j,k,dataset,NX,NY,NZ)).upper()
                if (k)%6 == 5:
                    ostr += "\n"
            ostr += "\n"
    return ostr