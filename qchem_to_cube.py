#!/usr/bin/env python
# coding: utf-8
import reader
import writer
import numpy as np
import sys

f1 = sys.argv[1]
if len(sys.argv) < 3:
    oname = "plot_{}.cube"
    f2 = None 
else:
    oname = "diffden_{}.cube"
    f2 = sys.argv[2]
detach = reader.read_plot(f1)
if f2 is not None:
    attach = reader.read_plot(f2)
else:
    attach = None
out = reader.read_output()

if f2 is not None:
    assert attach['ncol'] == detach['ncol']
    for i in range(3):
        assert abs(attach['origin'][i] - detach['origin'][i]) < 1E-12
        assert abs(attach['axes'][i] - detach['axes'][i]) < 1E-12
COMMENT1 = "Difference Density {}"
COMMENT2 = "baby yoda is top"
NVAL = detach['ncol']
NATOMS = out['natom']
ORIGIN = "{} {} {}".format(*detach['origin'])
NX = int(out["NX"])
NY = int(out["NY"])
NZ = int(out["NZ"])
C1 = detach['axes'][0]
C2 = detach['axes'][1]
C3 = detach['axes'][2]
XAXIS = f"{NX} {C1:6.10f} 0.0 0.0"
YAXIS = f"{NY} 0.0 {C2:6.10f} 0.0"
ZAXIS = f"{NZ} 0.0 0.0 {C3:6.10f}"
GEOM = out["gstr"]
for i in range(detach['ncol']):
    if f2 is not None:
        diff = np.array(attach['data'][i]) - np.array(detach['data'][i])
    else:
        diff = np.array(detach['data'][i])
    DATA = writer.create_data_ostr(diff,NX,NY,NZ)
    fdict = {"COMMENT1":COMMENT1.format(i),
             "COMMENT2":COMMENT2,
             "NATOMS":NATOMS,
             "ORIGIN":ORIGIN,
             "NVAL":"1",
             "XAXIS":XAXIS,
             "YAXIS":YAXIS,
             "ZAXIS":ZAXIS,
             "GEOM":GEOM,
             "DATA":DATA}
    output = writer.template.format(**fdict)
    with open("diffden_{}.cube".format(i),"w") as f: f.write(output)
