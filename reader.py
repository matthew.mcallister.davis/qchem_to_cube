import numpy as np
import sedre

def read_output(fname="output.dat"):
    """
    input:
        fname (string) name of Q-Chem output file
    output:
        ddict (dict)
            natom (int) num of atoms
            gstr (str) geometry in a.u. in +12.8f format
            NX (int) num. of voxel in x direction.
            NY (int) num. of voxel in Y direction.
            NZ (int) num. of voxel in Z direction
    notes:
        gstr entry in ddict is geometry after canonicalization,
            i.e. shifting COM and rotating principal axes
        gstr should be formatted like (W is nuclear charge)
            W W +X +Y +Z
    """
    autobohr = 1.88973
    myp = sedre.Parser(program="qchem",filename=fname)
    geom_array = myp.data['properties']['GEOM']['entry']['vals'][0]
    atlookup = {"H":1,"He":2,"Li":3,"Be":4,"B":5,"C":6,"N":7,"O":8,"F":9,"Ne":10}
    gstr = ""
    natom = len(geom_array)
    for line in geom_array:
        atom = line[0]
        atom = atlookup[atom]
        x = autobohr*float(line[1])
        y = autobohr*float(line[2])
        z = autobohr*float(line[3])
        gstr += f"{atom} {atom} {x:+12.8f} {y:+12.8f} {z:+12.8f}\n"
    with open(fname,"r") as f: cont = f.readlines()
    rflag1 = False
    rflag2 = False
    ct = 3
    pts = [None]*3
    for line in cont:
        if ct < 1:
            break
        if "$plots" in line:
            rflag1 = True
            continue
        if rflag1:
            rflag2 = True
            rflag1 = False
            continue
        if rflag2:
            pts[3-ct] = line.strip().split()[0]
            ct -= 1
            continue
    ddict = {"natom":natom,
             "gstr":gstr,
             "NX":pts[0],
             "NY":pts[1],
             "NZ":pts[2]}
    return ddict
def read_plot(fname):
    """
    input:
        fname (string) name of file
    output:
        ddict (dict)
            ncol (int) number of scalar fields
            data (list of lists of floats)
            fname (string) returns input fname
            origin (list of floats) negative extremum in all dimensions
    """
    def get_ncol(content):
        sline = content[3].strip().split()
        ncol = len(sline) - 3
        return ncol
    def get_origin(content):
        if "cut short" in content[0]:
            start = 1
        else:
            start = 3
        sline = content[start].strip().split()
        x = float(sline[0])
        y = float(sline[1])
        z = float(sline[2])
        return [x,y,z]
    def get_axis(content,col):
        sline1 = content[3].strip().split()
        val = float(sline1[col])
        for line in content[4:]:
            sline = line.strip().split()
            val2 = float(sline[col])
            if val != val2:
                return abs(val2 - val)
        #if function has not returned yet, there is an error
        raise("Axes could not be located!")
        
    try:
        with open(fname,"r") as f: cont = f.readlines()
    except:
        raise(f"File {fname} not found!")

    ncol = get_ncol(cont)
    assert ncol > 0, "Input file does not seem to contain data columns!"
    origin = get_origin(cont)
    xaxis = get_axis(cont,0)
    yaxis = get_axis(cont,1)
    zaxis = get_axis(cont,2)
    axes = [xaxis,yaxis,zaxis]
    data = []
    for i in range(ncol):
        data.append([])
    if "cut short" in cont[0]:
        start = 1
    else:
        start = 3
    for line in cont[start:]:
        sline = line.strip().split()
        for i in range(ncol):
            data[i].append(float(sline[i+3]))
    ddict = {"ncol":ncol,
             "data":data,
             "fname":fname,
             "origin":origin,
             "axes":axes}
    return ddict
